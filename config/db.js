// Import all modules used;
const mongoose = require('mongoose');

// Connect to mongoose database;
const DB_HOST = process.env.DB_HOST || 'mongodb://localhost:27017/tusko';
mongoose.connect(DB_HOST, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    // Optional authentication
    // user: 'your-username',
    // pass: 'your-password',
    // authSource: 'your-authSource' // Optional
});

module.exports = mongoose;