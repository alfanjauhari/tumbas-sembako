/**
 |--------------------------------------------------------------------------
 | Konfigurasi Aplikasi
 |--------------------------------------------------------------------------
 |
 | Semua konfigurasi yang berhubungan dengan aplikasi tumbas sembako ini
 | pastikan tidak terdapat informasi sensitif yang disimpan di sini. Simpan
 | informasi tersebut di file .env;
 |
 | @version 1.0.0
 | @author Alfan Jauhari <hi.alfanj@gmail.com>
 */

const application = {
  name: process.env.APP_NAME || 'Tumbas Sembako',
  port: process.env.APP_PORT || 3000,
  reservedUrl: ['create', 'update', 'delete']
}

module.exports = application;