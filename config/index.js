const db = require('./db');
const application = require('./application');

module.exports = { db, application }