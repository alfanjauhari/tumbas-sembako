/**
 |--------------------------------------------------------------------------
 | Product Controller
 |--------------------------------------------------------------------------
 |
 | Controller ini akan menghandle seluruh request user untuk mendapatkan
 | data-data produk di Tumbas Sembako;
 |
 | @version 1.0.0
 | @author Alfan Jauhari <hi.alfanj@gmail.com>
 */

const { response, isEmpty } = require('../libs/helpers');
const Product = require('../models/product');
const { reservedUrl } = require('../config/application');

/**
 * Mengambil semua data produk dari database
 * 
 * @param {any|any}
 * @return HTTP Response
 */
exports.getAllProducts = async (req, res) => {
  try {
    const data = await Product.find();

    if(isEmpty(data)) {
      const responseData = {
        code: 404,
        success: false,
        message: 'Failed get products data. Products not found!'
      }

      throw(responseData);
    }

    const responseData = {
      code: 200,
      success: true,
      message: 'Success get products data',
      content: data
    }

    return response(res, responseData);
  } catch(error) {
    return response(error);
  }
}

/**
 * Mengambil data produk dengan id dari database
 * 
 * @param {any|any|function}
 * @return mixed
 */
exports.getOneProduct = (req, res, next) => {
  try {
    /* Get id from request params */
    const { id } = req.params;

    if(reservedUrl.includes(id)) {
      next();
      return;
    }

    /* Get product by id */
    Product.findById(id, (error, doc) => {
      /* Cek terdapat error atau tidak saat mengeksekusi perintah findById */
      if(error) {
        const responseData = {
          code: 404,
          success: false,
          message: `Failed get product by id ${id}.`,
          content: {
            error
          }
        }

        return response(res, responseData);
      }

      const responseData = {
        code: 200,
        success: true,
        message: 'Success get product data',
        content: doc
      }

      return response(res, responseData);
    });
  } catch(error) {
    return error;
  }
}