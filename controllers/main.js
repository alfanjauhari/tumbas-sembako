/**
 |--------------------------------------------------------------------------
 | Main Controller
 |--------------------------------------------------------------------------
 |
 | Controller ini menghandle seluruh request dari user untuk menampilkan
 | informasi-informasi umum tentang aplikasi Tumbas Sembako;
 |
 | @version 1.0.0
 | @author Alfan Jauhari <hi.alfanj@gmail.com>
 */

const { response } = require('../libs/helpers');

/**
 * Welcome page;
 * 
 * @param {any|any}
 * @return HTTP Response
 */
exports.home = (req, res) => {
  const responseData = {
    code: 200,
    success: true,
    message: 'Hello from Tumbas Sembako! API Running Successfully. Let\'s Hacking!!'
  }

  return response(res, responseData);
}