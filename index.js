/**
 |--------------------------------------------------------------------------
 | Tumbas Sembako
 |--------------------------------------------------------------------------
 |
 | Tumbas sembako adalah sebuah aplikasi online shop menggunakan MEVN 
 | (MongoDB, ExpressJS, VueJS, NodeJS) Stack;
 |
 | @version 1.0.0
 | @author Alfan Jauhari <hi.alfanj@gmail.com>
 */

if(process.env.NODE_ENV !== 'production') require('dotenv').config();

const express = require('express');
const { db, application } = require('./config');
const app = express();

/* Router list */
const mainRouter = require('./routes/main');
const productRouter = require('./routes/product');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use('/', mainRouter);
app.use('/products', productRouter);

app.listen(application.port, () => console.log(`Tumbas sembako running on port ${application.port}`));

module.exports = app;