const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema;

const productSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'Please fill title data!']
  },
  slug: {
    type: String,
    required: [true, 'Please fill slug data!'],
    unique: true
  },
  image: {
    type: String,
    required: [true, 'Please fill image data!']
  },
  description: {
    type: String,
    required: [true, 'Please fill description data!']
  },
  price: {
    type: Number,
    required: [true, 'Please fill price data!']
  },
  sku: {
    type: String,
    required: [true, 'Please fill SKU data!'],
    unique: true
  },
  weight: {
    type: Number,
    required: [true, 'Please fill weight data!']
  },
  authorId: {
    type: ObjectId,
    required: [true, 'AuthorId data cant\' found!'],
    ref: 'User'
  },
  reviewsId: [{
    type: ObjectId,
    ref: 'Review'
  }]
});

module.exports = mongoose.model('Product', productSchema);