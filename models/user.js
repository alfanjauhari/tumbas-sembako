const mongoose = require('mongoose');
const { ObjectId } = mongoose.Schema;

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Please fill name data!']
  },
  username: {
    type: String,
    required: [true, 'Please fill username data!'],
    unique: true
  },
  email: {
    type: String,
    required: [true, 'Please fill email data!'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'Please fill password data!']
  },
  isSeller: {
    type: Boolean,
    default: false
  },
  productsId: [{
    type: ObjectId,
    ref: 'Product'
  }]
});

module.exports = mongoose.model('User', userSchema);