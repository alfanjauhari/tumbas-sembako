const supertest = require('supertest');
const server = require('../index.js');
const chai = require('chai');

chai.should();

const api = supertest.agent(server);

describe('connect to server', () => {
  it('should connect to server', done => {
    api.get('/')
      .end((err, res) => {
        res.status.should.equal(200);
        res.body.code.should.equal(200);
        done();
      });
  })
})