exports.response = (res, data) => {
  return res.status(data.code).json({
    code: data.code,
    success: data.success,
    message: data.message,
    content: data.content
  });
}

exports.isEmpty = (object) => {
  for(let key in object) {
    if(object.hasOwnProperty) {
      return false;
    }
  }

  return true;
}