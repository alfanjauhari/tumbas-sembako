// Impor semua modul yang digunakan;
const express = require('express');
const router = express.Router();
const { home } = require('../controllers/main');
const User = require('../models/user');

router.get('/', home);

module.exports = router;